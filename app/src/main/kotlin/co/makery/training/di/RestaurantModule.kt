package co.makery.training.di

import android.content.Context
import co.makery.training.BuildConfig
import co.makery.training.Constants
import co.makery.training.android.RestaurantApplication
import co.makery.training.service.AnalyticsServiceInterface
import co.makery.training.service.FirebaseAnalyticsService
import co.makery.training.service.GoogleAnalyticsService
import com.f2prateek.rx.preferences.RxSharedPreferences
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.LruCache
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.multibindings.ElementsIntoSet
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.defaultSharedPreferences
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class RestaurantModule(val ctx: Context) {

  @Provides @Singleton fun context() = ctx

  @Provides @Singleton fun retrofit(): Retrofit {
    val loggingInterceptor = HttpLoggingInterceptor()
      .apply { level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE }

    val client = OkHttpClient.Builder()
      .readTimeout(30, TimeUnit.SECONDS)
      .writeTimeout(30, TimeUnit.SECONDS)
      .addInterceptor(loggingInterceptor)
      .build()

    val retrofit = Retrofit.Builder()
      .baseUrl(Constants.baseUrl)
      .client(client)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
      .build()

    return retrofit
  }

  @Provides @Singleton fun picasso(ctx: Context): Picasso {
    val client = OkHttpClient().newBuilder()
      .connectTimeout(30, TimeUnit.SECONDS)
      .readTimeout(30, TimeUnit.SECONDS)
      .build()

    return Picasso.Builder(ctx)
      .downloader(OkHttp3Downloader(client))
      .memoryCache(LruCache(ctx))
      .build()
  }

  @Provides @Singleton fun rxSharedPreferences(ctx: Context): RxSharedPreferences {
    return RxSharedPreferences.create(ctx.defaultSharedPreferences)
  }

  @Provides @ElementsIntoSet fun analytics(ctx: Context): Set<AnalyticsServiceInterface> =
    when (BuildConfig.ANALYTICS_TYPE) {
      AnalyticsServiceInterface.Type.google -> setOf(GoogleAnalyticsService(ctx))
      AnalyticsServiceInterface.Type.firebase -> setOf(FirebaseAnalyticsService(ctx))
      else -> setOf(GoogleAnalyticsService(ctx), FirebaseAnalyticsService(ctx))
    }

  companion object {
    val instance = RestaurantModule(RestaurantApplication.instance)
  }
}
