package co.makery.training.di

import co.makery.training.android.activity.DetailsActivity
import co.makery.training.android.activity.LoginActivity
import co.makery.training.android.activity.MainActivity
import co.makery.training.android.widget.RecommendationView
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(RestaurantModule::class))
interface RestaurantComponent {

  fun inject(activity: MainActivity)
  fun inject(activity: DetailsActivity)
  fun inject(activity: LoginActivity)
  fun inject(view: RecommendationView)
}

class RestaurantComponentInjector {

  companion object {
    val instance: RestaurantComponent = DaggerRestaurantComponent.builder()
      .restaurantModule(RestaurantModule.instance)
      .build()
  }
}
