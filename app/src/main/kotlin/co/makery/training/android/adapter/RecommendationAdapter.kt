package co.makery.training.android.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.FrameLayout
import co.makery.training.android.widget.RecommendationView
import co.makery.training.model.RestaurantRecommendation
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.wrapContent

class RecommendationAdapter(ctx: Context, data: OrderedRealmCollection<RestaurantRecommendation>) :
  RealmRecyclerViewAdapter<RestaurantRecommendation, RecommendationViewHolder>(ctx, data, true) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendationViewHolder {
    return RecommendationViewHolder(
      RecommendationView(parent.context)
        .apply { layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent) }
    )
  }

  override fun onBindViewHolder(holder: RecommendationViewHolder, position: Int) {
    data?.get(position)?.let { item ->
      holder.recommendationView.setup(item)
    }
  }
}

class RecommendationViewHolder(val recommendationView: RecommendationView) : RecyclerView.ViewHolder(recommendationView)
