package co.makery.training.service

import android.content.Context
import co.makery.training.model.RestaurantRecommendation
import io.realm.Realm
import io.realm.RealmResults
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import rx.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiService {

  private val restaurantApi: RestaurantApi

  @Inject constructor(retrofit: Retrofit, ctx: Context) {
    restaurantApi = retrofit.create(RestaurantApi::class.java)
    Realm.init(ctx)
  }

  fun retrieveRecommendations(): Observable<RealmResults<RestaurantRecommendation>> {
    val realmList = Realm.getDefaultInstance().where(RestaurantRecommendation::class.java).findAll()
    if (realmList.isNotEmpty()) {
      return Observable.just(realmList)
    } else {
      return restaurantApi.getRestaurants()
        .map { restaurants ->
          val realm = Realm.getDefaultInstance()
          realm.executeTransaction {
            realm.copyToRealm(restaurants)
          }
          realmList
        }
    }
  }

  fun retrieveRestaurantDetails(id: String): Observable<RestaurantRecommendation> {
    return restaurantApi.getRestaurantDetails(id)
  }

  fun uploadRecommendation(recommendation: RestaurantRecommendation): Observable<Unit> {
    return restaurantApi.postRestaurantDetails(recommendation)
      .map { Unit }
  }
}

private interface RestaurantApi {
  @GET("recommendations") fun getRestaurants(): Observable<List<RestaurantRecommendation>>

  @GET("recommendation/{id}") fun getRestaurantDetails(@Path("id") id: String): Observable<RestaurantRecommendation>

  @POST("recommendations") fun postRestaurantDetails(@Body recommendation: RestaurantRecommendation): Observable<ResponseBody>
}
