package co.makery.training.android.widget

import android.content.Context
import android.support.v7.widget.CardView
import android.view.View
import android.view.ViewTreeObserver
import co.makery.training.R
import co.makery.training.android.activity.startDetailsActivity
import co.makery.training.di.RestaurantComponentInjector
import co.makery.training.model.RestaurantRecommendation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_recommendation.view.*
import org.jetbrains.anko.onClick
import javax.inject.Inject

class RecommendationView : CardView {

  companion object {
    var imageWidth = -1
    var imageHeight = -1
  }

  @Inject lateinit var picasso: Picasso
  private lateinit var recommendation: RestaurantRecommendation

  constructor(context: Context) : super(context) {
    init()
  }

  fun setup(recommendation: RestaurantRecommendation) {
    this.recommendation = recommendation

    titleText.text = recommendation.name
    userNameText.text = "${recommendation.user?.fullName}"

    shortDescText.text = recommendation.shortDesc

    starView.visibility = if (recommendation.liked) View.VISIBLE else View.GONE

    loadImage()

    onClick {
      startDetailsActivity(context, recommendation, recommendationImage)
    }
  }

  private fun init() {
    View.inflate(context, R.layout.view_recommendation, this)
    RestaurantComponentInjector.instance.inject(this)
  }

  private fun loadImage() {
    if (imageWidth == -1 && imageHeight == -1) {
      recommendationImage.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
        override fun onPreDraw(): Boolean {
          if (recommendationImage.height != 0 && recommendationImage.width != 0) {
            imageWidth = recommendationImage.width
            imageHeight = recommendationImage.height
            recommendationImage.viewTreeObserver.removeOnPreDrawListener(this)
            picasso.load(recommendation.imageUrl).resize(imageWidth, imageHeight).centerCrop().into(recommendationImage)
          }
          return true
        }
      })
    } else {
      picasso.load(recommendation.imageUrl).resize(imageWidth, imageHeight).centerCrop().into(recommendationImage)
    }
  }
}
