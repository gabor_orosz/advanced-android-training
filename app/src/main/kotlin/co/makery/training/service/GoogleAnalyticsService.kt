package co.makery.training.service

import android.content.Context
import co.makery.training.R
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker


class GoogleAnalyticsService(context: Context) : AnalyticsServiceInterface {

  private val tracker: Tracker

  init {
    tracker = GoogleAnalytics.getInstance(context).newTracker(R.xml.global_tracker)
  }

  override fun loginViewOpened() {
    tracker.setScreenName(AnalyticsServiceInterface.loginView)
    tracker.send(HitBuilders.ScreenViewBuilder().build())
  }

  override fun mainViewOpened() {
    tracker.setScreenName(AnalyticsServiceInterface.mainView)
    tracker.send(HitBuilders.ScreenViewBuilder().build())
  }

  override fun detailsViewOpened(id: String?) {
    tracker.setScreenName(AnalyticsServiceInterface.detailsView)
    tracker.send(HitBuilders.ScreenViewBuilder().build())
  }

  override fun detailsViewOpenedForNew() {
    tracker.setScreenName(AnalyticsServiceInterface.detailsViewNew)
    tracker.send(HitBuilders.ScreenViewBuilder().build())
  }

  override fun facebookLogin() {
    tracker.send(HitBuilders.EventBuilder()
      .setCategory(AnalyticsServiceInterface.loginCategory)
      .setAction(AnalyticsServiceInterface.facebook)
      .build())
  }

  override fun editModeStarted(id: String?) {
    tracker.send(HitBuilders.EventBuilder()
      .setCategory(AnalyticsServiceInterface.actionCategory)
      .setAction(AnalyticsServiceInterface.editModeStarted)
      .set(AnalyticsServiceInterface.idKey, id)
      .build())
  }

  override fun editModeFinished(id: String?) {
    tracker.send(HitBuilders.EventBuilder()
      .setCategory(AnalyticsServiceInterface.actionCategory)
      .setAction(AnalyticsServiceInterface.editModeFinished)
      .set(AnalyticsServiceInterface.idKey, id)
      .build())
  }

}