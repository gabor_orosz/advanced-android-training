package co.makery.training.android.activity

import android.graphics.Rect
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import co.makery.training.R
import co.makery.training.android.adapter.RecommendationAdapter
import co.makery.training.di.RestaurantComponentInjector
import co.makery.training.model.RestaurantRecommendation
import co.makery.training.model.StoredInfo
import co.makery.training.service.ApiService
import co.makery.training.service.AppAnalyticsService
import co.makery.training.service.FacebookService
import co.makery.training.service.PersisterService
import com.trello.rxlifecycle.components.support.RxAppCompatActivity
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header.view.*
import org.jetbrains.anko.dip
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class MainActivity : RxAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

  @Inject lateinit var apiService: ApiService
  @Inject lateinit var persisterService: PersisterService
  @Inject lateinit var facebookService: FacebookService
  @Inject lateinit var analyticsService: AppAnalyticsService

  private lateinit var adapter: RecommendationAdapter
  private lateinit var headerView: View
  private lateinit var loginItem: MenuItem
  private lateinit var logoutItem: MenuItem

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    RestaurantComponentInjector.instance.inject(this)

    setContentView(R.layout.activity_main)

    title = getString(R.string.main_title)
    setSupportActionBar(mainToolbar)

    val toggle = ActionBarDrawerToggle(this, drawerLayout, mainToolbar, R.string.open_drawer, R.string.close_drawer)
    drawerLayout.addDrawerListener(toggle)
    toggle.syncState()

    navigationView.setNavigationItemSelectedListener(this)

    recyclerListView.setHasFixedSize(true)
    recyclerListView.layoutManager = LinearLayoutManager(this)
    val margin = dip(16)
    val marginDecoration = object : RecyclerView.ItemDecoration() {
      override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val pos = parent.getChildAdapterPosition(view)
        if (pos == 0) {
          outRect.top += margin
        }
        outRect.bottom += margin
        outRect.left += margin
        outRect.right += margin
      }
    }
    recyclerListView.addItemDecoration(marginDecoration)

    progressView.visibility = View.VISIBLE
    apiService.retrieveRecommendations()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .doAfterTerminate {
        progressView.visibility = View.GONE
      }
      .compose(bindToLifecycle<RealmResults<RestaurantRecommendation>>())
      .subscribe(object : Observer<RealmResults<RestaurantRecommendation>> {
        override fun onError(e: Throwable?) {
          Log.e("ERROR", e?.message)
        }

        override fun onNext(resultList: RealmResults<RestaurantRecommendation>) {
          adapter = RecommendationAdapter(this@MainActivity, resultList)
          recyclerListView.adapter = adapter
        }

        override fun onCompleted() {
        }
      })

    addButton.onClick {
      startEmptyDetailsActivity(this)
    }

    headerView = navigationView.getHeaderView(0)
    loginItem = navigationView.menu.getItem(0)
    logoutItem = navigationView.menu.getItem(1)

    persisterService.storedInfoPref.asObservable()
      .observeOn(AndroidSchedulers.mainThread())
      .compose(bindToLifecycle<StoredInfo>())
      .subscribe { storedInfo ->
        if (storedInfo != null) {
          headerView.userNameText.text = storedInfo.userName
          headerView.userNameText.visibility = View.VISIBLE
          headerView.profilePictureView.profileId = storedInfo.userId
          headerView.profilePictureView.visibility = View.VISIBLE
          loginItem.isVisible = false
          logoutItem.isVisible = true
        } else {
          headerView.userNameText.visibility = View.GONE
          headerView.profilePictureView.visibility = View.GONE
          loginItem.isVisible = true
          logoutItem.isVisible = false
        }
      }
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.login -> startActivity<LoginActivity>()
      R.id.logout -> facebookService.logout()
    }
    drawerLayout.closeDrawer(GravityCompat.START)
    return true
  }

  override fun onResume() {
    super.onResume()
    analyticsService.mainViewOpened()
  }
}
