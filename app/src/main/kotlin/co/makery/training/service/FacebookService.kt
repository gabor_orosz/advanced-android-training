package co.makery.training.service

import android.app.Activity
import android.content.Intent
import co.makery.training.model.StoredInfo
import com.crashlytics.android.Crashlytics
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import rx.Observable
import rx.lang.kotlin.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FacebookService {

  private val loginManager = LoginManager.getInstance()
  private val callbackManager = CallbackManager.Factory.create()

  private val loginSubject = BehaviorSubject<StoredInfo>()

  private val persisterService: PersisterService

  @Inject constructor(persisterService: PersisterService) {
    loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
      override fun onError(error: FacebookException?) {
        loginSubject.onError(error)
      }

      override fun onSuccess(result: LoginResult) {
        GraphRequest.newMeRequest(result.accessToken) { me, response ->
          if (response.error != null) {
            loginSubject.onError(RuntimeException(response.error.errorMessage))
          } else {
            loginSubject.onNext(StoredInfo(me.optString("name"), me.optString("id"), result.accessToken.token))
          }
        }.executeAsync()
      }

      override fun onCancel() {
      }
    })

    this.persisterService = persisterService
  }

  fun login(activity: Activity): Observable<StoredInfo> {
    loginManager.logInWithReadPermissions(activity, listOf("public_profile"))
    return loginSubject
      .doOnError { error ->
        Crashlytics.logException(error)
      }
      .doOnNext { storedInfo ->
        persisterService.storedInfoPref.set(storedInfo)
      }
  }

  fun logout() {
    loginManager.logOut()
    persisterService.storedInfoPref.delete()
  }

  fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    callbackManager.onActivityResult(requestCode, resultCode, data)
  }
}
