package co.makery.training.service

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseAnalyticsService(context: Context) : AnalyticsServiceInterface {

  private val fireBaseAnalytics: FirebaseAnalytics

  init {
    fireBaseAnalytics = FirebaseAnalytics.getInstance(context)
  }

  override fun loginViewOpened() {
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.loginView, null)
  }

  override fun mainViewOpened() {
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.mainView, null)
  }

  override fun detailsViewOpened(id: String?) {
    val bundle = Bundle()
    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.detailsView, null)
  }

  override fun detailsViewOpenedForNew() {
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.detailsViewNew, null)
  }

  override fun facebookLogin() {
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.facebook, null)
  }

  override fun editModeStarted(id: String?) {
    val bundle = Bundle()
    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.editModeStarted, bundle)
  }

  override fun editModeFinished(id: String?) {
    val bundle = Bundle()
    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
    fireBaseAnalytics.logEvent(AnalyticsServiceInterface.editModeFinished, bundle)
  }

}