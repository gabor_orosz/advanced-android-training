package co.makery.training.android.activity

import android.content.Intent
import android.os.Bundle
import co.makery.training.R
import co.makery.training.android.snack
import co.makery.training.di.RestaurantComponentInjector
import co.makery.training.model.StoredInfo
import co.makery.training.service.AppAnalyticsService
import co.makery.training.service.FacebookService
import com.trello.rxlifecycle.android.ActivityEvent
import com.trello.rxlifecycle.components.support.RxAppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.onClick
import javax.inject.Inject

class LoginActivity : RxAppCompatActivity() {

  @Inject lateinit var facebookService: FacebookService
  @Inject lateinit var analyticsService: AppAnalyticsService


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    RestaurantComponentInjector.instance.inject(this)

    setContentView(R.layout.activity_login)

    fbButton.onClick {
      login()
    }

    analyticsService.loginViewOpened()
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    facebookService.onActivityResult(requestCode, resultCode, data)
  }

  private fun login() {
    analyticsService.facebookLogin()
    facebookService.login(this)
      .compose(bindUntilEvent<StoredInfo>(ActivityEvent.DESTROY))
      .subscribe({ result ->
        snack(result.accessToken)
        finish()
      }, Throwable::printStackTrace)
  }
}
