package co.makery.training.android

import android.app.Activity
import android.support.design.widget.Snackbar

fun Activity.snack(message: String, duration: Int = Snackbar.LENGTH_SHORT) {
  Snackbar.make(findViewById(android.R.id.content), message, duration).show()
}

fun Activity.snack(message: String, duration: Int = Snackbar.LENGTH_SHORT, actionText: String, action: () -> Unit) {
  Snackbar.make(findViewById(android.R.id.content), message, duration).setAction(actionText, { action() }).show()
}