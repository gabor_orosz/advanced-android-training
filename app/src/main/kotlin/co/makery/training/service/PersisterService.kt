package co.makery.training.service

import android.content.SharedPreferences
import co.makery.training.model.StoredInfo
import com.f2prateek.rx.preferences.Preference
import com.f2prateek.rx.preferences.RxSharedPreferences
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class PersisterService @Inject constructor(rxSharedPref: RxSharedPreferences) {

  companion object {
    val storedInfoKey = "sp.stored.info"
  }

  val storedInfoPref: Preference<StoredInfo>

  init {
    storedInfoPref = rxSharedPref.getObject(storedInfoKey, null, GsonPreferenceAdapter(Gson(), StoredInfo::class.java))
  }
}

private class GsonPreferenceAdapter<T>(val gson: Gson, val clazz: Class<T>) : Preference.Adapter<T> {

  override fun get(key: String, preferences: SharedPreferences): T {
    return gson.fromJson(preferences.getString(key, null), clazz)
  }

  override fun set(key: String, value: T, editor: SharedPreferences.Editor) {
    editor.putString(key, gson.toJson(value))
  }
}