package co.makery.training.model

class StoredInfo(val userName: String, val userId: String, val accessToken: String)