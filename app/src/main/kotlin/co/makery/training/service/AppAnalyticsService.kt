package co.makery.training.service

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppAnalyticsService @Inject constructor(val analyticsServices: Set<@JvmSuppressWildcards AnalyticsServiceInterface>) : AnalyticsServiceInterface {

  override fun loginViewOpened() {
    analyticsServices.forEach { it.loginViewOpened() }
  }

  override fun mainViewOpened() {
    analyticsServices.forEach { it.mainViewOpened() }
  }

  override fun detailsViewOpened(id: String?) {
    analyticsServices.forEach { it.detailsViewOpened(id) }
  }

  override fun detailsViewOpenedForNew() {
    analyticsServices.forEach { it.detailsViewOpenedForNew() }
  }

  override fun facebookLogin() {
    analyticsServices.forEach { it.facebookLogin() }
  }

  override fun editModeStarted(id: String?) {
    analyticsServices.forEach { it.editModeStarted(id) }
  }

  override fun editModeFinished(id: String?) {
    analyticsServices.forEach { it.editModeFinished(id) }
  }

}