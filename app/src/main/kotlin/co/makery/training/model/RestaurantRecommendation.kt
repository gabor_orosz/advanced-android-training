package co.makery.training.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class RestaurantRecommendation() : RealmObject() {

  constructor(id: String?,
              name: String?,
              shortDesc: String?,
              desc: String?,
              imageUrl: String?,
              user: User?,
              liked: Boolean = false) : this() {
    this.id = id
    this.name = name
    this.shortDesc = shortDesc
    this.desc = desc
    this.imageUrl = imageUrl
    this.user = user
    this.liked = liked
  }

  var id: String? = null

  var name: String? = null

  @SerializedName("short-desc")
  var shortDesc: String? = null

  var desc: String? = null

  @SerializedName("image-url")
  var imageUrl: String? = null

  var user: User? = null

  var liked: Boolean = false
}
