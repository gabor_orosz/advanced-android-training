package co.makery.training.service

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import okio.*
import java.io.ByteArrayOutputStream
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

enum class Method {
  Raw, Gzip, Base64, Base64Gzip
}

@Singleton
class ImageService @Inject constructor(ctx: Context) {

  private val parent = ctx.filesDir

  fun store(id: String, method: Method, bitmap: Bitmap): Unit {
    val file = newFile(id, method)
    when (method) {
      Method.Raw -> storeRaw(file, bitmap)
      Method.Gzip -> storeGzip(file, bitmap)
      Method.Base64 -> storeBase64(file, bitmap)
      Method.Base64Gzip -> storeBase64Gzip(file, bitmap)
    }
  }

  fun load(id: String, method: Method): Bitmap? {
    val file = newFile(id, method)
    if (!file.exists()) return null
    return when (method) {
      Method.Raw -> loadRaw(file)
      Method.Gzip -> loadGzip(file)
      Method.Base64 -> loadBase64(file)
      Method.Base64Gzip -> loadBase64Gzip(file)
    }
  }

  private fun storeRaw(file: File, bitmap: Bitmap): Unit {
    val buffer = Buffer()
    val bytes = bitmapBytes(bitmap)
    buffer.write(bytes)
    val sink = Okio.sink(file)
    buffer.readAll(sink)
    sink.close()
  }

  private fun loadRaw(file: File): Bitmap {
    val buffer = Buffer()
    val source = Okio.source(file)
    buffer.writeAll(source)
    source.close()
    val bitmap = BitmapFactory.decodeStream(buffer.inputStream())
    return bitmap
  }

  private fun storeGzip(file: File, bitmap: Bitmap): Unit {
    val buffer = Buffer()
    val bytes = bitmapBytes(bitmap)
    buffer.write(bytes)
    val sink = GzipSink(Okio.sink(file))
    buffer.readAll(sink)
    sink.close()
  }

  private fun loadGzip(file: File): Bitmap {
    val buffer = Buffer()
    val source = GzipSource(Okio.source(file))
    buffer.writeAll(source)
    source.close()
    val bitmap = BitmapFactory.decodeStream(buffer.inputStream())
    return bitmap
  }

  private fun storeBase64(file: File, bitmap: Bitmap): Unit {
    val buffer = Buffer()
    val bytes = bitmapBytes(bitmap)
    val byteString = ByteString.of(bytes, 0, bytes.size)
    val base64 = byteString.base64()
    buffer.writeUtf8(base64)
    val sink = Okio.sink(file)
    buffer.readAll(sink)
    sink.close()
  }

  private fun loadBase64(file: File): Bitmap {
    val buffer = Buffer()
    val source = Okio.source(file)
    buffer.writeAll(source)
    source.close()
    val base64 = buffer.readUtf8()
    val byteString = ByteString.decodeBase64(base64)
    val bytes = byteString.toByteArray()
    val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    return bitmap
  }

  private fun storeBase64Gzip(file: File, bitmap: Bitmap): Unit {
    val buffer = Buffer()
    val bytes = bitmapBytes(bitmap)
    val byteString = ByteString.of(bytes, 0, bytes.size)
    val base64 = byteString.base64()
    buffer.writeUtf8(base64)
    val sink = GzipSink(Okio.sink(file))
    buffer.readAll(sink)
    sink.close()
  }

  private fun loadBase64Gzip(file: File): Bitmap {
    val buffer = Buffer()
    val source = GzipSource(Okio.source(file))
    buffer.writeAll(source)
    source.close()
    val base64 = buffer.readUtf8()
    val byteString = ByteString.decodeBase64(base64)
    val bytes = byteString.toByteArray()
    val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    return bitmap
  }

  private fun newFile(id: String, method: Method) = File(parent, "$id-${method.name}")

  private fun bitmapBytes(bitmap: Bitmap): ByteArray {
    val outputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
    val bytes = outputStream.toByteArray()
    return bytes
  }
}
