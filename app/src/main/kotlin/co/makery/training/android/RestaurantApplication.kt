package co.makery.training.android

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import io.fabric.sdk.android.Fabric

class RestaurantApplication : Application() {

  companion object {
    lateinit var instance: Application
  }

  override fun onCreate() {
    super.onCreate()
    instance = this
    FacebookSdk.sdkInitialize(this)
    AppEventsLogger.activateApp(this)
    Fabric.with(this, Crashlytics())
  }
}