package co.makery.training.android.activity

import android.Manifest
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat
import android.support.v4.util.Pair
import android.support.v7.graphics.Palette
import android.support.v7.view.ActionMode
import android.transition.Fade
import android.view.Menu
import android.view.MenuItem
import android.view.View
import co.makery.training.R
import co.makery.training.android.snack
import co.makery.training.android.widget.RecommendationView
import co.makery.training.di.RestaurantComponentInjector
import co.makery.training.model.RestaurantRecommendation
import co.makery.training.model.User
import co.makery.training.service.ApiService
import co.makery.training.service.AppAnalyticsService
import co.makery.training.service.ImageService
import co.makery.training.service.Method
import com.flipboard.bottomsheet.commons.ImagePickerSheetView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.trello.rxlifecycle.components.support.RxAppCompatActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_details.*
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.onClick
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

fun startDetailsActivity(context: Context, recommendation: RestaurantRecommendation, view: View) {
  val sharedImagePair = Pair(view, context.getString(R.string.recommendation_image))
  var options =
    ActivityOptionsCompat.makeSceneTransitionAnimation(context as Activity, sharedImagePair)
  if (context is Activity) {
    val toolbar = context.findViewById(R.id.mainToolbar)
    val sharedToolbarPair = Pair(toolbar, context.getString(R.string.toolbar))
    options = ActivityOptionsCompat.makeSceneTransitionAnimation(context, sharedToolbarPair, sharedImagePair)
  }
  context.startActivity(
    Intent(context, DetailsActivity::class.java)
      .apply {
        putExtra(DetailsActivity.extraRecommendation, recommendation.id)
      },
    options.toBundle())
}

fun startEmptyDetailsActivity(context: Context) {
  context.startActivity(Intent(context, DetailsActivity::class.java), null)
}

class DetailsActivity : RxAppCompatActivity(), ActionMode.Callback, Target {

  companion object {
    val extraRecommendation = "extra.recommendation"
    val likeMenuItemId = 1
    val doneMenuItemId = 2
  }

  private val REQUEST_PERMISSIONS = 1
  private val REQUEST_IMAGE_CAPTURE = 2

  @Inject lateinit var picasso: Picasso
  @Inject lateinit var apiService: ApiService
  @Inject lateinit var imageService: ImageService
  @Inject lateinit var analyticsService: AppAnalyticsService

  private lateinit var recommendation: RestaurantRecommendation
  private lateinit var likeMenuItem: MenuItem

  private var inActionMode = false
  private var newRecommendation = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    RestaurantComponentInjector.instance.inject(this)

    window.setBackgroundDrawable(ColorDrawable(Color.WHITE))

    setContentView(R.layout.activity_details)
    title = getString(R.string.details)
    setSupportActionBar(detailsToolbar)

    detailsToolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp)
    detailsToolbar.setNavigationOnClickListener { ActivityCompat.finishAfterTransition(this) }

    val recId = intent.getStringExtra(extraRecommendation)
    val rec = Realm.getDefaultInstance().where(RestaurantRecommendation::class.java).equalTo("id", recId).findFirst()
    if (rec == null) {
      recommendation = RestaurantRecommendation(UUID.randomUUID().toString(),
        "Title",
        "short desc",
        "desc",
        null,
        User("first name", "last name"),
        false)
      startSupportActionMode(this)
      newRecommendation = true
      analyticsService.detailsViewOpenedForNew()
    } else {
      recommendation = rec
      analyticsService.detailsViewOpened(rec.id)
    }

    titleText.setText(recommendation.name)
    userNameText.text = recommendation.user?.fullName

    shortDescText.setText(recommendation.shortDesc)

    editButton.onClick {
      if (!inActionMode) {
        startSupportActionMode(this)
      }
    }

    editLayer.onClick {
      if (checkPermissions()) {
        showSheetView()
      } else {
        ActivityCompat.requestPermissions(this,
          arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
          REQUEST_PERMISSIONS)
      }
    }

    bottomSheet.peekOnDismiss = true

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      val fade = Fade()
      fade.excludeTarget(android.R.id.statusBarBackground, true)
      fade.excludeTarget(android.R.id.navigationBarBackground, true)
      window.exitTransition = fade
      window.enterTransition = fade
    }

    if (!newRecommendation) {
      val bitmap = imageService.load(recommendation.id!!, Method.Raw)
      if (bitmap != null) {
        recommendationImage.imageBitmap = bitmap
      } else {
        picasso.load(recommendation.imageUrl).resize(RecommendationView.imageWidth, RecommendationView.imageHeight).centerCrop().into(this)
      }
      loadDetails()
    } else {
      descText.setText(recommendation.desc)
    }
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    likeMenuItem = menu.add(Menu.NONE, likeMenuItemId, Menu.NONE, R.string.like)
    likeMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
    likeMenuItem.icon = if (recommendation.liked) ContextCompat.getDrawable(this, R.drawable.ic_star_white_24dp) else
      ContextCompat.getDrawable(this, R.drawable.ic_star_border_white_24dp)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      likeMenuItemId -> {
        val newLikedStatus = !recommendation.liked
        Realm.getDefaultInstance().executeTransaction {
          recommendation.liked = newLikedStatus
        }
        invalidateOptionsMenu()
        return true
      }
    }
    return false
  }

  override fun onCreateActionMode(mode: ActionMode?, menu: Menu): Boolean {
    val doneMenuItem = menu.add(Menu.NONE, doneMenuItemId, Menu.NONE, R.string.done)
    doneMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
    doneMenuItem.icon = ContextCompat.getDrawable(this, R.drawable.ic_done_white_24dp)
    return true
  }

  override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
    when (item.itemId) {
      doneMenuItemId -> {
        Realm.getDefaultInstance().executeTransaction {
          recommendation.apply {
            name = titleText.text.toString()
            shortDesc = shortDescText.text.toString()
            desc = descText.text.toString()
          }
        }
        mode.finish()
        if (newRecommendation) {
          uploadDetails()
        }
        return true
      }
    }
    return false
  }

  override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
    inActionMode = true

    titleText.isEnabled = true
    shortDescText.isEnabled = true
    descText.isEnabled = true

    titleText.background = ContextCompat.getDrawable(this, R.drawable.line)
    shortDescText.background = ContextCompat.getDrawable(this, R.drawable.line)
    descText.background = ContextCompat.getDrawable(this, R.drawable.line)

    editLayer.visibility = View.VISIBLE

    analyticsService.editModeStarted(recommendation.id)
    return true
  }

  override fun onDestroyActionMode(mode: ActionMode?) {
    inActionMode = false

    titleText.isEnabled = false
    shortDescText.isEnabled = false
    descText.isEnabled = false

    titleText.background = null
    shortDescText.background = null
    descText.background = null

    editLayer.visibility = View.GONE

    titleText.setText(recommendation.name)
    shortDescText.setText(recommendation.shortDesc)
    descText.setText(recommendation.desc)

    bottomSheet.dismissSheet()
    analyticsService.editModeFinished(recommendation.id)
  }

  override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
  }

  override fun onBitmapFailed(errorDrawable: Drawable?) {
  }

  override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
    if (bitmap != null) {
      recommendationImage.imageBitmap = bitmap
      generatePalette(bitmap)
    }
  }

  public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == REQUEST_IMAGE_CAPTURE) {
        val extras = data?.extras
        val bitmap = extras?.get("data") as Bitmap

        val id = recommendation.id!!

        imageService.store(id, Method.Raw, bitmap)
        recommendationImage.imageBitmap = imageService.load(id, Method.Raw)

        imageService.store(id, Method.Gzip, bitmap)
        recommendationImage.imageBitmap = imageService.load(id, Method.Gzip)

        imageService.store(id, Method.Base64, bitmap)
        recommendationImage.imageBitmap = imageService.load(id, Method.Base64)

        imageService.store(id, Method.Base64Gzip, bitmap)
        recommendationImage.imageBitmap = imageService.load(id, Method.Base64Gzip)
      }
    }
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
    if (requestCode == REQUEST_PERMISSIONS) {
      if (grantResults.size == 2 && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
        showSheetView()
      } else {
        snack(getString(R.string.permission_error))
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
  }

  private fun startColorAnimations(paletteColor: Int) {
    val toolbarAnimator =
      ObjectAnimator.ofObject(detailsToolbar,
        "backgroundColor",
        ArgbEvaluator(),
        ContextCompat.getColor(this, R.color.colorPrimary),
        paletteColor)

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      val darkerColor = createDarkerColor(paletteColor)

      val statusBarAnimator = ValueAnimator.ofInt(ContextCompat.getColor(this, R.color.colorPrimaryDark), darkerColor)
        .apply {
          setEvaluator(ArgbEvaluator())
          addUpdateListener { value ->
            window.statusBarColor = value.animatedValue as Int
          }
        }

      AnimatorSet().apply {
        duration = 1000
        playTogether(toolbarAnimator, statusBarAnimator)
        start()
      }
    } else {
      toolbarAnimator.apply {
        duration = 1000
        start()
      }
    }
  }

  private fun createDarkerColor(paletteColor: Int): Int {
    val hsv = FloatArray(3)
    Color.colorToHSV(paletteColor, hsv)
    hsv[2] *= 0.8f
    return Color.HSVToColor(hsv)
  }

  private fun generatePalette(bitmap: Bitmap) {
    Palette.from(bitmap)
      .generate { palette ->
        val paletteColor = palette.getDominantColor(ContextCompat.getColor(this, R.color.colorPrimary))
        editButton.backgroundTintList = ColorStateList.valueOf(paletteColor)
        startColorAnimations(paletteColor)
      }
  }

  private fun loadDetails() {
    progressView.visibility = View.VISIBLE
    recommendation.id?.let { id ->
      apiService.retrieveRestaurantDetails(id)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doAfterTerminate { progressView.visibility = View.GONE }
        .compose(bindToLifecycle<RestaurantRecommendation>())
        .subscribe(object : Observer<RestaurantRecommendation> {
          override fun onError(e: Throwable?) {
          }

          override fun onNext(details: RestaurantRecommendation) {
            descText.setText(details.desc)
          }

          override fun onCompleted() {
          }
        })
    }
  }

  private fun uploadDetails() {
    uploadProgressView.visibility = View.VISIBLE
    apiService.uploadRecommendation(recommendation)
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .doAfterTerminate { uploadProgressView.visibility = View.GONE }
      .compose(bindToLifecycle<Unit>())
      .subscribe(object : Observer<Unit> {
        override fun onError(e: Throwable?) {
        }

        override fun onNext(t: Unit) {
          Realm.getDefaultInstance().apply {
            executeTransaction {
              copyToRealm(recommendation)
            }
          }
          finish()
        }

        override fun onCompleted() {
        }
      })
  }

  private fun checkPermissions(): Boolean {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true
    return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
      ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
  }

  private fun showSheetView() {
    val sheetView = ImagePickerSheetView.Builder(this)
      .setShowCameraOption(createCameraIntent() != null)
      .setShowPickerOption(false)
      .setImageProvider { imageView, imageUri, size ->
      }
      .setOnTileSelectedListener { selectedTile ->
        bottomSheet.dismissSheet()
        if (selectedTile.isCameraTile) {
          dispatchTakePictureIntent()
        }
      }
      .setTitle(getString(R.string.choose_an_image))
      .create()

    bottomSheet.showWithSheetView(sheetView)
  }

  private fun createCameraIntent(): Intent? {
    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    if (takePictureIntent.resolveActivity(packageManager) != null) {
      return takePictureIntent
    } else {
      return null
    }
  }

  private fun dispatchTakePictureIntent() {
    val takePictureIntent = createCameraIntent()
    if (takePictureIntent != null) {
      startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
    }
  }
}
