package co.makery.training.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class User() : RealmObject() {

  constructor(firstName: String?, lastName: String?) : this() {
    this.firstName = firstName
    this.lastName = lastName
  }

  @SerializedName("first-name")
  var firstName: String? = null

  @SerializedName("last-name")
  var lastName: String? = null

  val fullName: String get() = "$firstName $lastName"
}