package co.makery.training.service

interface AnalyticsServiceInterface {

  object Type {
    val google = 1
    val firebase = 2
    val googleFirebase = 3
  }

  companion object {
    val loginView = "login"
    val mainView = "main"
    val detailsView = "details"
    val detailsViewNew = "detailsNew"

    val loginCategory = "details"
    val actionCategory = "action"

    val facebook = "facebook"
    val idKey = "id"
    val editModeStarted = "editModeStarted"
    val editModeFinished = "editModeFinished"
  }

  fun loginViewOpened()

  fun mainViewOpened()

  fun detailsViewOpened(id: String?)

  fun detailsViewOpenedForNew()

  fun facebookLogin()

  fun editModeStarted(id: String?)

  fun editModeFinished(id: String?)
}
